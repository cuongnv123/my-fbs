import React from 'react';
import { mySwitchNavigator} from './Component/AppNavigator';
import { createAppContainer } from 'react-navigation';

// const MyNavigation = createAppContainer(AppNavigator);
// const MyTabNavigator = createAppContainer(TabNavigator);
const SwitchNavigator = createAppContainer(mySwitchNavigator);

export default function App() {
  return (
    <>
        {/* <MyNavigation/> */}
        {/* <MyTabNavigator/> */}
        <SwitchNavigator/>
    </>  
  );
};