import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground , AsyncStorage, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import { Content, Icon, Picker, Form, Body, Item, Label, Input, Toast } from "native-base";
import GradientButton from 'react-native-gradient-buttons';
import background from '../assets/education.png';
import logoApp from '../assets/logoL.png';
import iconGoogle from '../assets/iconGoogle.png';
import arrow from '../assets/arrow.png';
import * as GoogleSignIn from 'expo-google-sign-in';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';

export default class ScreenAP extends React.Component {
    static navigationOptions = {
        headerLeft: null,
        headerBackground: () => (
            <GradientButton
                width='120%'
                radius={0}
                gradientBegin="#3542b3"
                gradientEnd="#313da5"
                style={{flex: 1, marginLeft: -20, color: 'white'}}
        />
        ),
    }

    constructor(props) {
        super(props);

        this.state = ({
            selected: 'fsbhn',
            user: null
        })
    }
    async onValueChange (value){
        this.setState({
            selected: value,
        });
    }

    componentDidMount() {
        // this.checkTokenLogin();
    }

    checkTokenLogin = async() => {
        const value = JSON.parse(await AsyncStorage.getItem('user'));
        if (value.accessToken && value.accessToken.length !== 0){
            this.props.navigation.navigate('HomeAP');
            return
        }
    }

    // signInAsync = async () => {
    //     try {
    //         const value = JSON.parse(await AsyncStorage.getItem('user'));
    //         await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
    //         if (value.accessToken && value.accessToken.length !== 0){
    //             this.props.navigation.navigate('HomeAP');
    //             return;
    //         }
    //         const result = await Google.logInAsync({
    //             ...GOOGLE_LOGIN,
    //             scopes: ['profile', 'email'],
    //         });
    //         if (result.type === 'success') {
    //             await AsyncStorage.setItem('user', JSON.stringify(result));
    //             // this.checkLoginUser(result);
    //             this.getUserInfo('son18mse13001')
    //         } else {
    //             console.log('cancel');
    //         }
    //     } catch (e) {
    //         console.log('cancel',e);
    //     }
    // }


    signInAsync = async () => {
        try {
            await GoogleSignIn.askForPlayServicesAsync();
            await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
            const result = await GoogleSignIn.signInAsync();
            if (result.type === 'success') {
                    AsyncStorage.setItem('user', JSON.stringify(result))
                    this.checkLoginUser(result);
            }
        } catch ({ message }) {
            console.log(message);
            alert('login: Error:' + message);
        }
      };

    handleLogout = async() => {
        try {
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('userLogin', JSON.stringify(''));
            await AsyncStorage.setItem('user', JSON.stringify(''));
        } catch (e) {
            console.log('cancel',e);
        }
    }

    blockLoginUser = () => {
        this.handleLogout();
        alert('Không tồn tại sinh viên này')
    }

    checkLoginUser = (info) => {
        if(info.user.email.indexOf('@fsb.edu.vn') === -1){
            this.blockLoginUser();
            return;
        }
        this.getUserInfo(info.user.email.replace('@fsb.edu.vn',''))
    }

    getUserInfo = async (userLogin) => {
        AsyncStorage.setItem('userLogin', JSON.stringify(userLogin))
        const url = `http://ap.fsb.edu.vn/api/getStudentInfo.php?campus_code=${this.state.selected}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then((responseJson) => {
            if (responseJson.success && responseJson.data.user_login){
                this.props.navigation.navigate('HomeAP');
            } else {
                this.blockLoginUser();
            }
        })
    }

    hardcodeLogin = async() => {
        await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
        this.getUserInfo('son19gem10048') 
    }

    onChangeText = (text) => {
        this.setState({
            user: text,
        })
    }

    render () {
        return (
            <KeyboardAvoidingView
                style={{flex: 1}}
                keyboardVerticalOffset={0}
                behavior="padding"
                enabled
            >
                <ImageBackground source={background} style={styles.myBackGround}>
                    <View style={styles.wrapper}>
                        <Image source={logoApp} style={styles.myLogo}/>
                    </View>
                    <View style={styles.wrapperContent}>
                        <Text style={styles.textItem}>Chọn cơ sở</Text>
                        <Text style={styles.campus}>Campus</Text>
                        <View style={{ backgroundColor: '#dcd4ff', width: '50%', borderRadius: 5, height: 50, marginTop: '23%', alignSelf: 'center'}}>
                            <Form>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Chọn cơ sở"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={styles.myPicker}
                                    selectedValue={this.state.selected}
                                    onValueChange={this.onValueChange.bind(this)}
                                    itemStyle={{fontWeight: 'bold', fontSize: 18}}
                                >
                                    <Picker.Item color='#f2701f' label="FSB-HÀ NỘI" value='fsbhn' />
                                    <Picker.Item color='#f2701f' label="FSB-ĐÀ NẴNG" value='fsbdn' />
                                    <Picker.Item color='#f2701f' label="FSB-HỒ CHÍ MINH" value='fsbhcm' />
                                    <Picker.Item color='#f2701f' label="FSB-CẦN THƠ" value='fsbcth' />
                                    <Picker.Item color='#f2701f' label="FCU FPT-MiniMBA" value='minimba' />
                                </Picker>
                            </Form>
                            <Image source={arrow} style={{width: 10, height: 10, position: 'absolute', right: 11, top: 20}} />
                        </View>
                        {/* <View style={styles.myButtonLogin}>
                            <GradientButton 
                                text="Đăng nhập"
                                gradientBegin="#ff631a"
                                gradientEnd="#f5a006"
                                gradientDirection="diagonal"
                                height={50}
                                width='77%'
                                radius={25}
                                impact
                                impactStyle='Heavy'
                                textStyle={{ color: 'black', fontSize: 18 }}
                                onPressAction={this.hardcodeLogin}
                            />
                        </View> */}
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <TouchableOpacity  onPress={this.signInAsync} style={styles.myButton}>
                            <Image source={iconGoogle} style={styles.myLogoButton}/>
                            <View style={{flex: 1, alignItems: 'center'}}>
                                <Text style={styles.textButton}>Sign in with Google</Text>
                                <Text style={styles.useEmail}>sử dụng email @fsb.edu.vn</Text>
                            </View>
                            
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    myBackGround: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        flex: 1,
    },
    myLogo: {
        width: '72%',
        resizeMode: 'contain',
        marginTop: '0%',
        marginBottom: 5,
    },
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderBottomEndRadius: 5,
        borderBottomStartRadius: 5,
    },
    wrapperContent: {
        flex: 3,
        justifyContent: 'center',
        marginBottom: 20,
    },
    textItem: {
        color: '#ffffff',
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    campus: {
        color: '#ffffff',
        paddingTop: 10,
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    textButton : {
        fontSize: 14,
        fontWeight: 'bold',
        marginRight: 20,
        color: '#3441b2'
    },
    useEmail : {
        fontSize: 10,
        marginRight: 20,
        color: 'red'
    },
    myPicker: {
        color: '#f2701f',
        fontWeight: 'bold',
        fontSize: 15,
        backgroundColor: 'transparent',
        width: '100%',
    },
    myButton: {
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        alignItems: 'center',
        backgroundColor: 'white',
        width: '72%', 
        height: 65, 
        borderRadius: 5,
        marginTop: 20,
    },
    myButtonLogin: {
        justifyContent: 'center', 
        alignItems: 'center',
        marginTop: '12%'
    },
    myLogoButton: {
        width: 35, 
        height: 35,
        marginLeft:20
    }
});
