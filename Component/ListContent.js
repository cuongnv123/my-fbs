import React from 'react';
import { Container, Content, Thumbnail, Text, Left, Body, Card, CardItem, Icon } from 'native-base';
import { StyleSheet, View, TouchableHighlight, FlatList, ActivityIndicator, RefreshControl, AsyncStorage, Image} from 'react-native';
import thumbnailDefault from '../assets/IconLogo.png';

export default class ListContent extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            isLoading: false,
            target: this.props.target || 1,
        };
    }

    setModalVisible(item){
        this.props.navigation.navigate('ListContentDetail',{ title: item.title, content: item});
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.target !== this.props.target){
            this.setState({
                dataSource: [],
                target: nextProps.target,
                isLoading: false,
            }, () => {
                this.getData();
            })
        }
    }
    
    componentDidMount(){
        this._isMounted = true;
        this.getData();
    }

    getData = async () => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const url = `http://ap.fsb.edu.vn/api/getNewsByCategory.php?campus_code=${campusCode}&cat_id=${this.state.target}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            if (!this._isMounted){
                return;
            }
            responseJson.data.sort((a,b) => {
                let timeA = new Date(a.modified.replace(' ','T')).getTime();
                let timeB = new Date(b.modified.replace(' ','T')).getTime();
                if (!timeA){
                    timeA = new Date(a.created.replace(' ','T')).getTime();
                }
                if (!timeB){
                    timeB = new Date(b.created.replace(' ','T')).getTime();
                }
                return timeB - timeA;
            })

            this.setState({
                dataSource: responseJson.data,
                isLoading: true,
            })
        }) 
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _onRefresh = async () => {
        await this.setState({
            dataSource: [],
        });
        await this.getData();
    }

    renderFooter = () => {
        if (this.state.isLoading){
            return <Text style={{height: 0}}></Text>;
        }
       return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
            <ActivityIndicator size="large" color="#f36523"/>
        </View>
       )
    }

    renderRow (item) {
        const thumbnai = item.image && item.image.length !== 0 ? { uri: `${item.image}`} : thumbnailDefault;
        return (
            <Card>
                <TouchableHighlight  onPress={() => this.setModalVisible(item)}>
                    <CardItem>
                            <Left>
                                <Image style={{width: 70,height: 70}} source={thumbnai}></Image>
                                <Content style={styles.contentCard}>
                                    <View >
                                        <Text style={styles.textTitle}>{item.title}</Text>
                                    </View>
                                    <View style={styles.contentDetail}>
                                        <Icon name="person" style={styles.iconItem}/>
                                        <Text style={styles.textItem}>{item.created_by}</Text>
                                        <Icon name="clock" style={styles.iconItem}/>
                                        <Text style={styles.textItem}>{item.created}</Text>
                                    </View>
                                </Content>
                            </Left>
                    </CardItem>
                </TouchableHighlight>
            </Card>
        )
    }

    render () {
        return (
            <Container>
                <FlatList
                    data={this.state.dataSource}
                    keyExtractor={(item,index) => index.toString()}
                    // onEndReached={this.loadMoreData}
                    // onEndReachedThreshold ={0.3}
                    renderItem={({item}) => this.renderRow(item)}
                    ListFooterComponent={ this.renderFooter}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={this._onRefresh}
                        />}
                />
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginLeft: 10,
    },
    contentDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    textItem: {
        color: '#cccccc',
        fontSize: 13,
        marginRight: 5,
    },
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: -2,
        marginRight: 5,
    },
    textTitle: {
        marginBottom: 5,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        minHeight: 50,
        fontSize: 13,
        color: '#3441b2'
    }
});
