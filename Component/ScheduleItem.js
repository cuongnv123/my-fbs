import React from 'react';
import {  Thumbnail, Text, List, ListItem, Left, Body, Icon, Container} from 'native-base';
import { StyleSheet, View, FlatList, ActivityIndicator, RefreshControl, AsyncStorage} from 'react-native';
import GradientButton from 'react-native-gradient-buttons';

export default class ScheduleItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            isLoading: false,
            selectedDay: this.props.selectedDay,
        };
    }

    componentDidMount(){
        this.getData();
    }

    componentWillReceiveProps(nextProps){
        if (this.props.selectedDay !== nextProps.selectedDay){
            this.setState({
                selectedDay: nextProps.selectedDay,
                dataSource: [],
                isLoading: false,
            }, () => {
                this.getData();
            })
        }
    }

    getData = async () => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));
        const url = `http://ap.fsb.edu.vn/api/getScheduleInfo.php?campus_code=${campusCode}&user_login=${userLogin}&num_of_day=${this.state.selectedDay}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                dataSource: responseJson.data,
                isLoading: true,
            })
        }) 
    }

    renderRow = ({item}) => {
        return (
            <ListItem thumbnail>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <GradientButton
                        text={item.group_name}
                        gradientBegin="#ff631a"
                        gradientEnd="#f5a006"
                        width={70}
                        height={80}
                        radius={10}
                        impact
                        textStyle={{ color: 'black', fontSize: 11 }}
                    />
                </View>
                <Body style={{flex: 4}}>
                    <Text style={{fontSize: 17, fontWeight: 'bold'}}>{item.subject_name}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="calendar" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.date}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="clock" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.slot}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="business" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.room_name.replace('<br/>', '')}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="person" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.leader_login}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="laptop" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.subject_code}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name="locate" style={styles.iconItem}/>
                        <Text note style={styles.textItem}>{item.area_name}</Text>
                    </View>
                </Body>
            </ListItem>
        )
    }

    _onRefresh = async () => {
        await this.setState({
            dataSource: [],
        });
        await this.getData();
    }

    render () {
        if (!this.state.isLoading){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator  size="large" color="#f36523" />
                </View>
            );
        }
        return (
            <Container>
                <List>
                    <FlatList
                        data={this.state.dataSource}
                        keyExtractor={(item,index) => index.toString()}
                        renderItem={(item) => this.renderRow(item)}
                        refreshControl={
                            <RefreshControl
                                refreshing={false}
                                onRefresh={this._onRefresh}
                            />}
                    />
                </List>
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 2,
        marginRight: 10,
    },
});
