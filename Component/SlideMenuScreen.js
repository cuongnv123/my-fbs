import React from 'react';
import { StyleSheet, View, ImageBackground, Image, Text, AsyncStorage, ActivityIndicator, TouchableOpacity} from 'react-native';
import { List, ListItem, Left, Icon} from 'native-base';
import background from '../assets/logoL.png';
// import { USER } from '../constants/hardcodeUser';

export default class SlideMenuScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'HomeAP',
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            myUser: {},
        }
    }

    async componentDidUpdate(){
        const value = JSON.parse(await AsyncStorage.getItem('user'));

        if (value && (!Object.keys(this.state.myUser).length || value.user.email !== this.state.myUser.user.email)){
            this.setState({
                myUser: value,
            })
        }
        // this.setState({
        //     myUser: USER
        // })
    }

    handleRouter(screen,param){
        this.props.navigation.closeDrawer();
        this.props.navigation.navigate(screen,param);
    }

    render () {
        const { myUser } = this.state;
        if (!Object.keys(myUser).length){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator  size="large" color="#f36523" />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <View style={styles.headerSlide}>
                    <ImageBackground source={background} style={styles.myBackGround}>
                        <TouchableOpacity style={styles.headerInfo} onPress={() => this.handleRouter('Profile',{ isHeaderHome: true})}>
                            <View style={styles.headerAvatar}>
                                <Image source={{uri: `${myUser.user.photoURL}`}} style={styles.myImage}></Image>
                            </View>
                            <View style={styles.headerText}>
                                <Text style={{fontWeight: 'bold', color: '#3441b2'}}>{myUser.user.displayName}</Text>
                                <Text style={styles.textName}>{ myUser.user.email }</Text>
                            </View>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={styles.contentSlide}>
                    <List>
                        <ListItem noIndent button onPress={() => this.handleRouter('HomeAP',{ isStartHome: true})}>
                            <Left>
                                <Icon name="home" style={styles.contentIcon}/>
                                <Text style={styles.textName}>Home</Text>
                            </Left>
                        </ListItem>
                        <ListItem noIndent itemDivider>
                            <Left>
                                <Text style={styles.textTitle}>News</Text>
                            </Left>
                        </ListItem>  
                        <ListItem noIndent button onPress={() => this.handleRouter('ListNewsStudy')}>
                            <Left>
                                <Icon name="book" style={styles.contentIcon}/>
                                <Text style={styles.textName}>Tin FSB</Text>
                            </Left>
                        </ListItem>  
                        <ListItem noIndent button  onPress={() => this.handleRouter('ListNewsAction')}>
                            <Left>
                                <Icon name="alert" style={styles.contentIcon}/>
                                <Text style={styles.textName}>Tin Đào Tạo</Text>
                            </Left>
                        </ListItem>  
                        {/* <ListItem noIndent button onPress={() => this.handleRouter('ListNewsFee')}>
                            <Left>
                                <Icon name="card" style={styles.contentIcon}/>
                                <Text>Học phí</Text>
                            </Left>
                        </ListItem>   */}
                        <ListItem noIndent itemDivider>
                            <Left>
                                <Text style={styles.textTitle}>Profile</Text>
                            </Left>
                        </ListItem>  
                        <ListItem noIndent button onPress={() => this.handleRouter('ExamSchedule')}>
                            <Left>
                                <Icon name="business" style={styles.contentIcon} />
                                <Text style={styles.textName}>Schedule</Text>
                            </Left>
                        </ListItem>  
                        <ListItem noIndent button onPress={() => this.handleRouter('Attendance',{ isHeaderHome: true })}>
                            <Left>
                                <Icon name="checkmark" style={styles.contentIcon}/>
                                <Text style={styles.textName}>Attendance</Text>
                            </Left>
                        </ListItem>  
                        <ListItem noIndent button onPress={() => this.handleRouter('HistoryLesson')}>
                            <Left>
                                <Icon name="calendar" style={styles.contentIcon} />
                                <Text style={styles.textName}>Mark</Text>
                            </Left>
                        </ListItem>  
                        {/* <ListItem noIndent button onPress={() => this.handleRouter('TuitionFee')}>
                            <Left>
                                <Icon name="cash" style={styles.contentIcon}/>
                                <Text>Finance</Text>
                            </Left>
                        </ListItem>   */}
                        {/* <ListItem noIndent button onPress={() => this.handleRouter('Bonus')}>
                            <Left>
                                <Icon name="archive" style={styles.contentIcon}/>
                                <Text>Khen thưởng/ Kỷ luật</Text>
                            </Left>
                        </ListItem>   */}
                        <ListItem noIndent button onPress={() => this.handleRouter('Introduction')}>
                            <Left>
                                <Text style={styles.textTitle}>About As</Text>
                            </Left>
                        </ListItem>  
                    </List>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerSlide: {
        flex: 1,
    },
    contentSlide: {
        flex: 3,
    },
    myBackGround: {
        height: '55%',
        resizeMode: 'contain',
        flex: 1,
        marginTop: 15,
        backgroundColor: 'powderblue',
        justifyContent: "flex-end",
    },
    headerInfo: {
       flexDirection: 'row',
       justifyContent: 'space-around',
       marginBottom : 10,
    },
    headerAvatar: {
        flex: 4,
    },
    myImage: {
        width: 50,
        height: 50,
        borderRadius: 30,
        overflow: 'hidden',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    headerText: {
        flex: 9,
        marginLeft: -5,
        justifyContent: 'space-around',
    },
    contentIcon: {
        marginRight: 10,
        color: '#3441b2'
    },
    textName: {
        color: '#3441b2',
    },
    textTitle: {
        color: '#3441b2',
        fontWeight: 'bold',
        fontSize: 15,
    }
});
