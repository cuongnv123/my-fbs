import React from 'react';
import { Container, Text} from 'native-base';
import { StyleSheet, View, AsyncStorage, Image } from 'react-native';
import { Agenda } from 'react-native-calendars';
import HeaderSelectDay from './HeaderSelectDay';
import moment from 'moment';
import iconArea from '../assets/iconHome.png';
import iconDate from '../assets/iconDate.png';
import iconRoom from '../assets/iconRoom.png';
import iconSubject from '../assets/iconSubject.png';
import iconClass from '../assets/iconClass.png';
import iconPerson from '../assets/iconPerson.png';
import iconSlot from '../assets/iconSlot.png';
import arrowDown from '../assets/arrowDown.png';

export default class Schedule extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            dataCalendar:{},
            selectedAgendaDay: {},
        };
    }

    async componentDidMount(){
        await this.getData(90);
    }

    getData = async (selectedDay) => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));
        const url = `http://ap.fsb.edu.vn/api/getScheduleInfo.php?campus_code=${campusCode}&user_login=${userLogin}&num_of_day=${selectedDay}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                dataSource: this.state.dataSource.concat(responseJson.data),
            })
        }) 
    }

    convertDataCalendar = (data) => {
        if (!data.length){
            return;
        }
        const objectData = Object.assign({},data)
        Object.keys(objectData).map((keys) => {
            const dateEvent = objectData[keys].date_fomat;
            objectData[keys] = [objectData[keys]];
            if (!objectData[dateEvent]){
                Object.defineProperty(objectData,dateEvent,
                    Object.getOwnPropertyDescriptor(objectData, keys));
                delete objectData[keys];
            } else {
                objectData[dateEvent].push(objectData[keys][0]);
            }

            if (!objectData[dateEvent][0].subject_code){
                objectData[dateEvent] = [];
            }
        })
        return objectData;
    }

    onDayPress = (day) => {
        this.setState({
            selectedAgendaDay: day,
        })
    }

    renderItem = (item, day) => {
        // if (this.state.selectedAgendaDay.dateString !== item.date_fomat){
        //     return;
        // }
        return (
            <View style={[styles.item]}>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconArea} style={styles.iconTitle}/>  {item.area_name}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconDate} style={styles.iconTitle}/>  {item.date}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconRoom} style={styles.iconTitle}/>  {item.room_name && item.room_name.replace(/Classroom:|<br ?\/?>/g,'')}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconSubject} style={styles.iconTitle}/>  {item.subject_name}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconClass} style={styles.iconTitle}/>  {item.group_name}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconPerson} style={{width: 18, height: 18,  resizeMode: 'contain'}}/>  {item.leader_login}
                </Text>
                <Text style={{ color: '#6a6a6a' }}>
                    <Image source={iconSlot} style={{width: 18, height: 18,  resizeMode: 'contain'}}/>  {item.slot}
                </Text>
            </View>
        );
      };
    
    rowHasChanged = (r1, r2) => {
        return (r1.day !== r2.day || r1.location !== r2.location || r1.name !== r2.name || r1.user !== r2.user);
    };

    render () {
        const dataCalandar = this.convertDataCalendar(this.state.dataSource);
        const dayNow = moment().format('YYYY-MM-DD');
        const maxDate = moment(moment().add(90, 'days').calendar(),'MM-DD-YYYY').format('YYYY-MM-DD');
        const minDate = moment(moment().subtract(90, 'days').calendar(),'MM-DD-YYYY').format('YYYY-MM-DD')
        return (
            <Container>
                <HeaderSelectDay selected={false} title="LỊCH HỌC"/>
                <View style={{flex: 1}}>
                    <Agenda
                        items={dataCalandar}
                        onDayPress={this.onDayPress}
                        renderEmptyDate={this.renderEmptyDate}
                        rowHasChanged={this.rowHasChanged}
                        renderItem={this.renderItem}
                        maxDate={maxDate}
                        minDate={minDate}
                        renderKnob={() =>  (<View><Image source={arrowDown} style={{width: 13, height: 13, marginVertical: 5}} /></View>)}
                        selected={dayNow}
                        renderEmptyData = {() => <View style={{flex: 1, justifyContent: "center", alignItems: 'center'}}><Text style={{fontWeight: 'bold', fontSize: 19, color: '#3441b2'}}>Không có lịch học vào hôm nay</Text></View>}
                        pastScrollRange={50}
                        futureScrollRange={50}
                        renderEmptyDate={() => <View />}
                        theme={{
                          'stylesheet.calendar.header': {
                            week: { marginTop: -2, flexDirection: 'row', justifyContent: 'space-between' }
                          }
                        }}
                    />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginTop: 15,
    },
    iconTitle: {
        width: 18, 
        height: 18,  
        resizeMode:'contain',
    }
  });
  
