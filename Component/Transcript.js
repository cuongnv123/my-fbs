import React from 'react';
import * as GoogleSignIn from 'expo-google-sign-in';
import { Container, Content, Text, Left, Header } from 'native-base';
import { StyleSheet, AsyncStorage,View } from 'react-native';
import HeaderSelectDay from './HeaderSelectDay';
import myHeader from './HeaderHome';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';
import HistoryLesson from './HistoryLesson';

export default class Transcript extends React.Component {

    static navigationOptions = ({ navigation }) => {
        if (navigation.getParam('isHeaderHome')){
            return myHeader("Bảng điểm", navigation);
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isVisible: false,
        };
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        if (this.props.navigation.getParam('isHeaderHome'))
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        const { navigation } = this.props;

        return (
            <Container>
                { navigation.getParam('isHeaderHome') ? <Header hasTabs transparent /> : <HeaderSelectDay selected={false} onValueChange={this.onValueChange} title="BẢNG ĐIỂM"/>}
                <HistoryLesson navigation={this.props.navigation} isHome/>
                { !navigation.getParam('isHeaderHome') && <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/> }
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginLeft: 10,
    },
    contentDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textItem: {
        color: '#cccccc',
        fontSize: 13,
    },
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 2,
    },
    textTitle: {
        marginBottom: 5,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        minHeight: 50,
        fontSize: 14,
    }
});
