import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image,ImageBackground} from 'react-native';
import GradientButton from 'react-native-gradient-buttons';
import iconAP from '../assets/iconAP.png';
import iconLMS from '../assets/iconLMS.png';
import iconEform from '../assets/iconEform.png';
import iconOnline from '../assets/iconOnline.png';
import iconErule from '../assets/iconErule.png';
import iconEdoc from '../assets/iconEdoc.png';
import iconEschedule from '../assets/iconEschedule.png';
import iconEasd from '../assets/iconEasd.png';
import LogoHeader from '../assets/LogoHeader.png';
import Background from '../assets/background.png';
import * as WebBrowser from 'expo-web-browser';

export default class Home extends React.Component {
    static navigationOptions = {
        title: '',
        headerBackground: () => (
            <GradientButton
                width='120%'
                radius={0}
                gradientBegin="#3542b3"
                gradientEnd="#1d265f"
                style={{flex: 1, marginLeft: -20, color: 'white'}}
        />
        ),
    }

    constructor(props) {
        super(props);
        this.state = {
            target: 1,
        }
    }

    handleWebview(url){
        WebBrowser.openBrowserAsync(url, {showTitle : true});
    }

    handleActive(target){
        this.setState({
            target,
        })
    }

    render () {
        const { navigation } = this.props;

        return (
            <ImageBackground source={Background} style={{ width: '100%', height: '100%'}} imageStyle={{opacity: 0.8, height: '80%',resizeMode: 'cover', marginTop: '50%'}}>
                <View style={{flex: 1,zIndex: 99}}>
                    <View style={{flex: 1}}>
                        <View style={{flex: 1, marginHorizontal: 35, marginBottom: 10}}>
                            <Image source={LogoHeader} style={{width: '100%', height: 70, resizeMode: 'contain'}}/>
                        </View>
                        <View style={styles.container}>
                            <TouchableOpacity style={this.state.target === 1 ? styles.activeTabLeft : styles.contentLeft} onPress={() => this.handleActive(1)}>
                                    <Text style={this.state.target === 1 ? styles.textAtive : styles.textContent}>Student</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={this.state.target === 2 ? styles.activeTabRight : styles.contentRight} onPress={() => this.handleActive(2)}>
                                    <Text style={this.state.target === 2 ? styles.textAtive : styles.textContent}>Faculty & Staff</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 9, marginTop: 20, marginHorizontal: 15, zIndex: 10}}>
                                <View style={styles.myItemStaff}>
                                    <TouchableOpacity onPress={() => navigation.navigate('ScreenAP')} style={styles.contentItem}>
                                        <Image source={iconAP} style={styles.myImage}/>
                                        <Text style={{alignSelf:'center', color: '#3441b2',fontWeight: 'bold'}}>AP</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.handleWebview('http://lms.fsb.edu.vn/')} style={styles.contentItem}>
                                        <Image source={iconLMS} style={styles.myImage}/>
                                        <Text style={{alignSelf:'center', color: '#3441b2',fontWeight: 'bold'}}>LMS</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.myItemStaff}>
                                    <TouchableOpacity onPress={() => this.handleWebview('http://eform.fsb.edu.vn/')} style={styles.contentItem}>
                                        <Image source={iconEform} style={styles.myImage}/>
                                        <Text style={{alignSelf:'center',color: '#3441b2',fontWeight: 'bold'}}>eFORM</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.handleWebview('http://online.fsb.edu.vn/')} style={styles.contentItem}>
                                        <Image source={iconOnline} style={styles.myImage}/>
                                        <Text style={{alignSelf:'center',color: '#3441b2',fontWeight: 'bold'}}>eLEARNING</Text>
                                    </TouchableOpacity>
                                </View>
                                { this.state.target === 2 && 
                                    <>
                                        <View style={styles.myItemStaff}>
                                            <TouchableOpacity onPress={() => this.handleWebview('http://erule.fsb.edu.vn/')} style={styles.contentItem}>
                                                <Image source={iconErule} style={styles.myImage}/>
                                                <Text style={{alignSelf:'center',justifyContent: 'center',alignItems: 'center',color: '#3441b2',fontWeight: 'bold'}}>eRULE</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.handleWebview('http://edoc.fsb.edu.vn/')} style={styles.contentItem}>
                                                <Image source={iconEdoc} style={styles.myImage}/>
                                                <Text style={{alignSelf:'center',color: '#3441b2',fontWeight: 'bold'}}>eDOC</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.myItemStaff}>
                                            <TouchableOpacity onPress={() => this.handleWebview('http://easd.fsb.edu.vn/')} style={styles.contentItem}>
                                                <Image source={iconEasd} style={styles.myImage}/>
                                                <Text style={{alignSelf:'center',color: '#3441b2',fontWeight: 'bold'}}>eASD</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.handleWebview('http://eschedule.fsb.edu.vn/')} style={styles.contentItem}>
                                                <Image source={iconEschedule} style={styles.myImage}/>
                                                <Text style={{alignSelf:'center',color: '#3441b2',fontWeight: 'bold'}}>ESCHEDULE</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                                }
                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    listStaff: {
        flex: 2,
    },
    myItemStaff: {
        height: '22%',
        height: 85,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 15,
    },
    contentItem: {
        width: '40%',
        justifyContent: 'center',
        alignItems: 'center', 
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#b3b3b3',
    },
    myImage : {
        width: 40,
        height: 40,
        resizeMode: 'contain',
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentLeft: {
        backgroundColor: '#e9e9e9',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    contentRight: {
        backgroundColor: '#e9e9e9',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    textContent: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#cccccc',
    },
    textAtive: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
    },
    activeTabLeft: {
        backgroundColor: '#f97c14',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
    },
    activeTabRight: {
        backgroundColor: '#f97c14',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    }
});
