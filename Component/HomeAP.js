import React from 'react';
import * as Font from "expo-font";
import { StyleSheet,Image, AsyncStorage, } from 'react-native';
import { Container, Header, Icon} from 'native-base';
import * as GoogleSignIn from 'expo-google-sign-in';
import TabFooter from './Footer';
import ListNews from './ListNews';
import Schedule from './Schedule';
import Attendance from './Attendance';
import Transcript from './Transcript';
import Roboto from '../node_modules/native-base/Fonts/Roboto.ttf';
import RobotoMedium from '../node_modules/native-base/Fonts/Roboto_medium.ttf';
import background from '../assets/logoM.png';
import Profile from './Profile';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';
import GradientButton from 'react-native-gradient-buttons';
 

export default class HomeAP extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const {params = {}} = navigation.state;
        return {
            title: '',
            headerBackground: () => (
                <GradientButton
                    width='120%'
                    radius={0}
                    gradientBegin="#3542b3"
                    gradientEnd="#1d265f"
                    impact
                    style={{flex: 1, marginLeft: -20}}
            />
            ),
            headerTransparent: true,
            // headerLeft: () => (
            //     <Icon style={{color: 'white'}} ios='ios-menu' android="md-menu" onPress={() => {navigation.toggleDrawer()}}/>
            // ),
            headerRight: () => (
                <Icon style={{color: 'white'}} type="FontAwesome" name='sign-out' onPress={() => params.onLogout()}/>
            ),
            headerStyle: {
                marginHorizontal: 10,
            },
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            targetPage: '1',
            fontLoaded: false,
            isVisible: false,
            targetPage: '1',
        }
    }

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: Roboto,
            Roboto_medium: RobotoMedium,
        });
        this.setState({ fontLoaded: true })
    }

    componentDidMount(){
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    componentDidUpdate(prevProps, prevState){
        if (this.props.navigation.getParam('isStartHome')&& prevState.targetPage !== '1'){
            this.setState({
                targetPage: '1',
            })
            this.props.navigation.setParams({isStartHome: false});
        }
    }

    actionNavigation = () => {
        this.props.navigation.push('profile');
    }

    actionPageFooter = (targetPage) => {
        this.setState({
            targetPage,
        })
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    render () {
        if (!this.state.fontLoaded) {
            return (
                <></>
            );
        }

        return (
            <Container>
                <Header hasTabs transparent />
                { this.state.targetPage === '1' && <ListNews navigation={this.props.navigation}/> }
                { this.state.targetPage === '2' && <Schedule navigation={this.props.navigation}/> }
                { this.state.targetPage === '3' && <Profile navigation={this.props.navigation}/> }
                { this.state.targetPage === '4' && <Attendance navigation={this.props.navigation}/> }
                { this.state.targetPage === '5' && <Transcript navigation={this.props.navigation}/> }
                <TabFooter actionPageFooter={this.actionPageFooter} target={this.state.targetPage}/>

                <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});
