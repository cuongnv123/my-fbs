import React from 'react';
import { Icon } from 'native-base';
import GradientButton from 'react-native-gradient-buttons';

export default myHeader = (title,navigation) => {
    const {params = {}} = navigation.state;
    return {
        title: title,
        headerTransparent: true,
        headerBackground: () => (
            <GradientButton
                width='120%'
                radius={0}
                gradientBegin="#3542b3"
                gradientEnd="#1d265f"
                impact
                style={{flex: 1, marginLeft: -20, color: 'white'}}
                textStyle = {{}}
        />
        ),
        // headerLeft: () => (
        //     <Icon style={{color: 'white'}} ios='ios-menu' android="md-menu" onPress={() => {navigation.toggleDrawer()}}/>
        // ),
        headerRight: () => (
            <Icon style={{color: 'white'}} type="FontAwesome" name='sign-out' onPress={() => params.onLogout()}/>
        ),
        headerStyle: {
            marginHorizontal: 10,
        },
        headerTitleStyle: {
            color: 'white'
          }
    }
 }