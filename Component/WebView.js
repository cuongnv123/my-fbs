import React, { Component } from 'react';
import { StyleSheet, View} from 'react-native';
import { WebView } from 'react-native-webview';
import * as WebBrowser from 'expo-web-browser';


export default class MyWebView extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('nameItem'),
        }
    }

    constructor(props) {
        super(props);
    }

    async componentWillMount(){
        let result = await WebBrowser.openBrowserAsync('https://expo.io');
    }

    render () {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                {/* <WebView source={{ uri: `${(navigation.getParam('url'))}`}} /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
       flex: 1,
    }
 })
