import React from 'react';
import { AsyncStorage } from 'react-native';
import * as GoogleSignIn from 'expo-google-sign-in';
import { Container, Content, Header } from 'native-base';
import { StyleSheet } from 'react-native';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';
import HistoryLessionItem from './HistoryLessionItem';

export default class HistoryLesson extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return  myHeader('Mark', navigation)
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selected: 0,
            isVisible: false,
        };
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    onValueChange = (value) => {
        this.setState({
          selected: value
    })}

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        if(this.props.isHome){
            return;
        }
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        return (
            <Container>
                {!this.props.isHome && <Header hasTabs transparent />}
                <HistoryLessionItem navigation={this.props.navigation}/>
                {!this.props.isHome && <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>}
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});
