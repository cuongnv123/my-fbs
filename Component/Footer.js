import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Platform } from 'react-native';
import { Footer, FooterTab, Icon, Text,View } from 'native-base';
import BottomNavigation, {
    FullTab
} from 'react-native-material-bottom-navigation';


const tabs = [
    {
        key: '1',
        icon: 'home',
        label: 'Home',
        barColor: '#ffffff',
        pressColor: '#3441b28f',
        size: 22,
    },
    {
        key: '2',
        icon: 'eye',
        label: 'Schedule',
        barColor: '#ffffff',
        pressColor: '#3441b28f',
        size: 22,
    },
    {
        key: '3',
        icon: 'ios-person',
        label: '',
        barColor: '#ffffff',
        pressColor: '#3441b28f',
        size: 50,
    },
    {
        key: '4',
        icon: 'checkmark',
        label: 'Attendance',
        barColor: '#ffffff',
        pressColor: '#3441b28f',
        size: 22,
    },
    {
        key: '5',
        icon: 'clipboard',
        label: 'Mark',
        barColor: '#ffffff',
        pressColor: '#3441b28f',
        size: 22,
    }
]

export default class TabFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            target: this.props.target,
        }
    }

    handleTouchFooter(target){
        this.props.actionPageFooter(target);
        this.setState({
            target,
        })
    }

    componentWillReceiveProps(nextProps) {
       if (this.state.target !== nextProps.target){
           this.setState({
               target: nextProps.target,
           })
       }
    }

    renderIcon = tab => ({ isActive }) => {
        return (
            <Icon style={[isActive ? {color: '#3441b2'} : {color: '#cccccc'}, {fontSize: tab.size}]} name={tab.icon} />
        )
    }

     
    renderTab = ({ tab, isActive }) => (
        <FullTab
            isActive={isActive}
            key={tab.key}
            label={tab.label}
            renderIcon={this.renderIcon(tab)}
            labelStyle={[{color: '#657db2', fontWeight: 'bold'},
                tab.key === this.props.target && { fontWeight: 'bold', color: '#3441b2'},
            ]}
            style={[
                tab.key === '3' && styles.styleTabUser,
                styles.tabFull,
            ]}

        />
    )

    render() {
        const { target } = this.state;
        return (
            <BottomNavigation
                activeTab={target}
                onTabPress={newTab => this.props.actionPageFooter(newTab.key)}
                renderTab={this.renderTab}
                tabs={tabs}
            />
        );
    }
}

const styles = StyleSheet.create({
    styleTabUser: {
        paddingTop: 0,
        // paddingBottom: 30,
    },
    // tabFull: {
    //     minWidth: 60, 
    // },
});


