import React from 'react';
import { Header,Title } from 'native-base';
import { View,Image } from 'react-native';
import LogoHeader from '../assets/LogoHeader.png';

export default class HeaderTitle extends React.Component {
    render () {
        return (
            <Header androidStatusBarColor='white' style={{ backgroundColor: 'white', flexDirection: 'column', height: 110, marginTop: 10,}} noShadow>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}>
                        <Image source={LogoHeader} style={{width: '80%', height: 70, resizeMode: 'contain'}}/>
                    </View>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#f97c14', marginHorizontal: 20, marginBottom: 15, borderRadius: 3}}>
                        <Title style={{color: 'white',  fontSize: 16, fontWeight: 'bold', marginLeft: 10}}>{this.props.title}</Title>
                    </View>
                </View>
            </Header>
        );
    }
}
