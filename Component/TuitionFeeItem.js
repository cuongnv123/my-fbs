import React from 'react';
import { StyleSheet, View, ImageBackground, Image, ActivityIndicator } from 'react-native';
import { Text, List, ListItem, Content } from 'native-base';
import background from '../assets/logoL.png';
import ImageCreditCard from '../assets/credit-card.png';

export default class TuitionFeeItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {
        const { userInfo } = this.props;
        if (!Object.keys(userInfo).length){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator  size="large" color="#f36523" />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <View style={{flex: 1}}>
                    <View style={styles.headerSlide}>
                        <View style={styles.headerInfo}>
                            <View style={styles.headerUserInfo}>
                                <Text note>Số dư: </Text>
                                <Text style={{fontSize: 22, fontWeight: 'bold'}}>{userInfo.address.zipcode} (VNĐ) </Text>
                                <Text note>Lịch sử phí :</Text>
                                <Text style={styles.textName}>Đã hoàn thành học phí đến kỳ 4</Text>
                                <Text note>Trạng thái : </Text>
                                <Text style={styles.textName}>HDI (Học đi)</Text>
                            </View>
                            <View style={styles.headerImage}>
                                <Image source={ImageCreditCard} style={styles.myImage}></Image>
                            </View>
                        </View>
                        <ImageBackground source={background} style={styles.myBackGround}>
                        </ImageBackground>
                    </View>
                    <View style={{flex: 2,justifyContent: 'center', alignItems: 'center'}}>
                        <Text note style={{fontSize: 18}}> Phí dự kiến học kỳ tiếp theo</Text>
                        <Text style={{fontSize: 22, fontWeight: 'bold'}}> 5.555.555</Text>
                    </View>
                </View>
                <Content style={styles.contentSlide}>
                    <List>
                        <ListItem itemDivider >
                            <Text style={styles.textName}>Phí sách</Text>
                        </ListItem>
                        <ListItem >
                            <Text>Số tiền: 1.000.000</Text>
                        </ListItem>
                        <ListItem >
                            <Text>Số lượng: 1</Text>
                        </ListItem>
                        <ListItem itemDivider>
                            <Text style={styles.textName}>Phí học kỳ</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Số tiền: 4.000.000</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Số lượng: 1</Text>
                        </ListItem>
                        <ListItem itemDivider>
                            <Text style={styles.textName}>Phí vệ sinh</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Số tiền: 555.555</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Số lượng: 1</Text>
                        </ListItem>
                    </List>
                </Content>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    headerSlide: {
        flex: 3,
        backgroundColor: '#0fdede',
        paddingBottom: 15,
        marginHorizontal: 10,
        borderRadius: 20,
    },
    contentSlide: {
        flex: 3, 
    },
    myBackGround: {
        height: '100%',
        resizeMode: 'cover',
        opacity: 0.1,
        position: 'relative',
        bottom: 0,
        backgroundColor: '#0fdede',
    },
    headerUserInfo: {
        flex: 3,
        justifyContent: 'flex-start',
    },
    headerInfo: {
        flex: 3,
        justifyContent: 'flex-start',
        flexDirection: 'row', 
        marginHorizontal: 23, 
        alignItems: 'flex-start',
        marginTop: 10,
    },
    headerImage: {
        flex: 1,
        justifyContent: 'flex-start',
    },
    myImage: {
        width: 70,
        height: 70,
    },
    contentIcon: {
        marginRight: 10,
    },
    textName: {
        fontWeight: 'bold',
        fontSize: 17, 
    },
});
