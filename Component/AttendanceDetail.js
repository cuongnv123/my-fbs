import React from 'react';
import { List, ListItem, Text, Left} from 'native-base';
import { StyleSheet, View, ActivityIndicator, AsyncStorage, FlatList, RefreshControl} from 'react-native';

export default class AttendanceDetail extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return  {
            title: 'Chi tiết điểm danh',
        }
    }

    constructor(props) {
        super(props);
    }


    _onRefresh = async () => {
        await this.setState({
            isLoading: false,
        });
    }

    renderRow = ({item}) => {
        return (
            <>
                <ListItem itemDivider style={{justifyContent: 'space-between'}}>
                        <Text style={{fontWeight: 'bold'}}>{item.DATE}</Text>
                        <Text style={ item.STATUS === 'Absent' ? styles.textAbsent: styles.textPresent}>{item.STATUS}</Text>
                </ListItem>                  
                <View style={{marginLeft: 15}}>
                    <Text>SLOT : {item.SLOT && item.SLOT}</Text>
                    <Text>CSD : {item.CSD && item.CSD}</Text>
                    <Text>TYPE : {item.TYPE && item.TYPE}</Text>
                    <Text>NOTE : {item.NOTE && item.NOTE}</Text>
                </View>
            </>
        )
    }

    render () {
        const content = this.props.navigation.getParam('content');

        if (!content.detail){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
                    <Text>Không có dữ liệu</Text>
                </View>
            )
        }

        return (
            <View style={{flex: 1, marginHorizontal: 20, marginTop: 20 }}>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}><Text style={styles.textHeader}>{ content.subject_name }</Text></View>
                    <View style={{marginHorizontal: 20}}>
                        <Text style={{fontWeight: 'bold',fontSize: 14}}>Vắng : <Text style={{color: 'red'}}>{`${content.attendance_absent}/${content.total_session}     ${content.percent_absent_total_session}%`}</Text> trên tổng số </Text>
                        <Text style={{fontWeight: 'bold',fontSize: 14}}>Vắng : <Text style={{color: 'red'}}>{`${content.attendance_absent}/${content.total_to_now}     ${content.percent_absent_total_to_now}%`}</Text> tới hiện tại</Text>
                    </View>
                </View>
                <View style={{flex: 5, marginTop: 25 }}>
                    <List>
                        <FlatList
                            data={content.detail}
                            keyExtractor={(item,index) => index.toString()}
                            renderItem={(item) => this.renderRow(item)}
                            refreshControl={
                                <RefreshControl
                                    refreshing={false}
                                    onRefresh={this._onRefresh}
                                />}
                        />
                    </List>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textPresent: {
        fontWeight: 'bold',
        color: 'green',
    },
    textAbsent: {
        fontWeight: 'bold',
        color: 'red',
    },
    textHeader: {
        fontWeight: 'bold',
        fontSize: 14,
    }
});
