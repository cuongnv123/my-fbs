import React from 'react';
import { Tab, Tabs, TabHeading, Text, } from 'native-base';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import ListContent from './ListContent';
import LogoHeader from '../assets/LogoHeader.png';

export default class ListNews extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            target: 1,
        };
    }

    handleActive(target){
        this.setState({
            target,
        })
    }

    render () {
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1, marginHorizontal: 35, marginBottom: 10}}>
                    <Image source={LogoHeader} style={{width: '100%', height: 70, resizeMode: 'contain'}}/>
                </View>
                <View style={styles.container}>
                    <TouchableOpacity style={this.state.target === 1 ? styles.activeTabLeft : styles.contentLeft} onPress={() => this.handleActive(1)}>
                            <Text style={this.state.target === 1 ? styles.textAtive : styles.textContent}>Tin FSB</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={this.state.target === 2 ? styles.activeTabRight : styles.contentRight} onPress={() => this.handleActive(2)}>
                            <Text style={this.state.target === 2 ? styles.textAtive : styles.textContent}>Tin Đào Tạo</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 9, marginTop: 20}}>
                    <ListContent navigation={this.props.navigation} target={this.state.target}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentLeft: {
        backgroundColor: '#e9e9e9',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    contentRight: {
        backgroundColor: '#e9e9e9',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    textContent: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#cccccc',
    },
    textAtive: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
    },
    activeTabLeft: {
        backgroundColor: '#f97c14',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
    },
    activeTabRight: {
        backgroundColor: '#f97c14',
        padding: 10,
        width: '45%',
        alignItems: 'center',
        marginTop: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    }
});
