export const GOOGLE_LOGIN = {
    // behavior: 'web',
    iosClientId: '403991143434-i8v6pc58qfb40vr5a8jii5qm3jql3m5d.apps.googleusercontent.com',
    androidClientId: '403991143434-k32fkobov40imqi7eni62vutg6rr4jjj.apps.googleusercontent.com',
    iosStandaloneAppClientId: '403991143434-d9lqv7503467b0pggtbbstaiu1lmujqv.apps.googleusercontent.com',
    androidStandaloneAppClientId: '403991143434-keiau099tii73fmhumb5p5eant6qk2ll.apps.googleusercontent.com',
    expoClientId: '403991143434-6lph9ic6kg8h8ee4p4hgqua0vsqeppng.apps.googleusercontent.com',
};